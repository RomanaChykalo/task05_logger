package hello.example;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
public class ExampleSMS {
    // i find my Account Sid and Token at twilio.com/user/account
    public static final String ACCOUNT_SID = "ACbe0720cb72846dae5e3f7d11a4fb1b2e";
    public static final String AUTH_TOKEN = "a952c34f129ce80af80bf99b4ef120f3";
    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380937597167"), /*to me*/
                        new PhoneNumber("+15034066020"), str) .create(); /*from somebody*/
    }
}
